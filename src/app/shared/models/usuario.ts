export class Usuario {
  id: number;
  name: string;
  email: string;
  cue_ult_sesion: string;
  cue_estado: number;
  created_at: string;
  updated_at: string;
}
