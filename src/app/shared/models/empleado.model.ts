export class Empleado {
  idempleado: number;
  nombres: string;
  ap_paterno: string;
  ap_materno: string;
  cargo_idcargo: number;
  complejo_idcomplejo: number;
  idcargo: number;
  cargo: string;
}
