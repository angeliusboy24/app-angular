import { Injectable } from '@angular/core';

@Injectable()
export class Env {
  private readonly URL_BASE: string;
  private ENDPOINTS;

  public login: string;
  public empleado: string;
  public cargo: string;
  public equipamento: string;

  constructor() {
    this.URL_BASE = 'http://localhost:8000/api';
    enum endpoint {
      Login = '/login',
      Empleado = '/empleado',
      Cargo = '/cargo',
      Equipamento = '/equipamento',
    }
    this.ENDPOINTS = endpoint;

    this.login = this.URL_BASE + this.ENDPOINTS.Login;
    this.empleado = this.URL_BASE + this.ENDPOINTS.Empleado;
    this.cargo = this.URL_BASE + this.ENDPOINTS.Cargo;
    this.equipamento = this.URL_BASE + this.ENDPOINTS.Equipamento;

  }
}
