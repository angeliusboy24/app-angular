import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Env} from '../../env';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {

  constructor(
    private http: HttpClient,
    private env: Env
  ) {
  }

  listar(): Observable<any> {
    return this.http.get(this.env.empleado);
  }

  crear(data): Observable<any> {
    return this.http.post(this.env.empleado, data);
  }

  actualizar(id, data): Observable<any> {
    return this.http.put(`${this.env.empleado}/${id}`, data);
  }

  empleadoId(id): Observable<any> {
    return this.http.get(`${this.env.empleado}/${id}`);
  }

  eliminar(id): Observable<any> {
    return this.http.delete(`${this.env.empleado}/${id}`);
  }


}
