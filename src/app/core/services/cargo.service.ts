import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Env} from '../../env';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Cargo} from '../../shared/models/cargo';

@Injectable({
  providedIn: 'root'
})
export class CargoService {

  constructor(
    private http: HttpClient,
    private env: Env
  ) {
  }

  listar(): Observable<any> {
    return this.http.get(this.env.cargo).pipe(map((res: Cargo) => {
      return res;
    }));
  }

}
