import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Env} from '../../env';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EquipamentoService {

  constructor(
    private http: HttpClient,
    private env: Env
  ) {
  }

  listar(): Observable<any> {
    return this.http.get(this.env.equipamento);
  }

  crear(data): Observable<any> {
    return this.http.post(this.env.equipamento, data);
  }

  actualizar(id, data): Observable<any> {
    return this.http.put(`${this.env.equipamento}/${id}`, data);
  }

  equipamentoId(id): Observable<any> {
    return this.http.get(`${this.env.equipamento}/${id}`);
  }

  eliminar(id): Observable<any> {
    return this.http.delete(`${this.env.equipamento}/${id}`);
  }
}
