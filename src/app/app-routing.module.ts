import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './modules/web/login/login.component';
import { AuthGuard } from './core/guards/auth.guard';
import { DashboardComponent } from './modules/intranet/dashboard/dashboard.component';
import { LogoutComponent } from './modules/intranet/logout/logout.component';
import {EmpleadoComponent} from './modules/intranet/empleado/empleado.component';
import {EmpleadoDetailsComponent} from './modules/intranet/empleado-details/empleado-details.component';
import {AddEmpleadoComponent} from './modules/intranet/add-empleado/add-empleado.component';
import {EquipamentoComponent} from './modules/intranet/equipamento/equipamento.component';
import {AddEquipamentoComponent} from './modules/intranet/add-equipamento/add-equipamento.component';
import {EquipamentoDetailsComponent} from './modules/intranet/equipamento-details/equipamento-details.component';

const routes: Routes = [
  {path:  '', pathMatch:  'full', redirectTo:  'login'},
  {path: 'login', component: LoginComponent},
  /*{path: 'dashboard', component: DashboardComponent},
  {path: 'logout', component: LogoutComponent},*/
  {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
  {path: 'logout', component: LogoutComponent, canActivate: [AuthGuard]},
  {path: 'empleados', component: EmpleadoComponent, canActivate: [AuthGuard]},
  {path: 'equipamentos', component: EquipamentoComponent, canActivate: [AuthGuard]},
  { path: 'empleado/:id', component: EmpleadoDetailsComponent, canActivate: [AuthGuard]},
  { path: 'equipamento/:id', component: EquipamentoDetailsComponent, canActivate: [AuthGuard]},
  { path: 'add-empleado', component: AddEmpleadoComponent, canActivate: [AuthGuard]},
  { path: 'add-equipamento', component: AddEquipamentoComponent, canActivate: [AuthGuard]},
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
