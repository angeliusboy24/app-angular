import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutModule } from '@angular/cdk/layout';
import { LoginComponent } from './modules/web/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ApiInterceptor } from './core/interceptors/api.interceptor';
import { ErrorInterceptor } from './core/interceptors/error.interceptor';
import { Env } from './env';
import { DashboardComponent } from './modules/intranet/dashboard/dashboard.component';
import { LogoutComponent } from './modules/intranet/logout/logout.component';
import { EmpleadoComponent } from './modules/intranet/empleado/empleado.component';
import { AddEmpleadoComponent } from './modules/intranet/add-empleado/add-empleado.component';
import { EmpleadoDetailsComponent } from './modules/intranet/empleado-details/empleado-details.component';
import { EquipamentoComponent } from './modules/intranet/equipamento/equipamento.component';
import { AddEquipamentoComponent } from './modules/intranet/add-equipamento/add-equipamento.component';
import { EquipamentoDetailsComponent } from './modules/intranet/equipamento-details/equipamento-details.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    LogoutComponent,
    EmpleadoComponent,
    AddEmpleadoComponent,
    EmpleadoDetailsComponent,
    EquipamentoComponent,
    AddEquipamentoComponent,
    EquipamentoDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    LayoutModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    Env,
    Title
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
