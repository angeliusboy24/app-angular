import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../../../core/authentication/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
  ) {
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/dashboard']);
    }
  }

  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email, Validators.minLength(10)]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  get f(): any { return this.loginForm.controls; }

  onSubmit(): any {
    this.submitted = true;
    if (this.loginForm.invalid) {
      this.loading = false;
      return;
    } else {
      this.loading = true;
    }

    console.log(this.f.email.value);
    console.log(this.f.password.value);

    this.authenticationService.login(this.f.email.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          if (data.success === true) {
            this.loading = false;
            this.router.navigate(['/dashboard']);
          } else {
            this.loading = false;
          }
        },
        error => {
          this.loading = false;
        });
  }
}
