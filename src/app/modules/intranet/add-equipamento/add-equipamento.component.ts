import { Component, OnInit } from '@angular/core';
import {Equipamento} from '../../../shared/models/equipamento';
import {EquipamentoService} from '../../../core/services/equipamento.service';

@Component({
  selector: 'app-add-equipamento',
  templateUrl: './add-equipamento.component.html',
  styleUrls: ['./add-equipamento.component.css']
})
export class AddEquipamentoComponent implements OnInit {

  equipamento: Equipamento;
  submitted = false;

  constructor(
    private equipamentoService: EquipamentoService) { }

  ngOnInit(): any {
    this.equipamento = {
      idequipamento: null,
      equipamento: ''
    };

  }

  crearEquipamento(): void {
    const data = {
      equipamento: this.equipamento.equipamento,
    };
    console.log(data);
    this.equipamentoService.crear(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
        },
        error => {
          console.log(error);
        });
  }

  nuevoEquipamento(): void {
    this.submitted = false;
    this.equipamento = {
      idequipamento: null,
      equipamento: ''
    };
  }

}
