import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { Empleado } from '../../../shared/models/empleado.model';
import { Cargo } from '../../../shared/models/cargo';
import { EmpleadoService } from '../../../core/services/empleado.service';
import { CargoService } from '../../../core/services/cargo.service';

@Component({
  selector: 'app-add-empleado',
  templateUrl: './add-empleado.component.html',
  styleUrls: ['./add-empleado.component.css']
})
export class AddEmpleadoComponent implements OnInit {

  empleado: Empleado;
  cargo: Cargo[];
  submitted = false;

  constructor(
    private empleadoService: EmpleadoService,
    private cargoService: CargoService) { }

  ngOnInit(): any {
    this.empleado = {
      idempleado: null,
      nombres: '',
      ap_paterno: '',
      ap_materno: '',
      cargo_idcargo: null,
      complejo_idcomplejo: null,
      idcargo: null,
      cargo: ''
    };

    this.cargoService.listar().pipe(first()).subscribe(cargo => {
      if (cargo.success === true && cargo.data) {
        this.cargo = cargo.data;
        console.log(this.cargo);
      }
    }, error => {
      console.log(error);
    });

  }

  crearEmpleado(): void {
    const data = {
      nombres: this.empleado.nombres,
      ap_paterno: this.empleado.ap_paterno,
      ap_materno: this.empleado.ap_materno,
      cargo_idcargo: this.empleado.cargo_idcargo,
      complejo_idcomplejo: this.empleado.complejo_idcomplejo
    };
    console.log(data);
    this.empleadoService.crear(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
        },
        error => {
          console.log(error);
        });
  }

  nuevoEmpledo(): void {
    this.submitted = false;
    this.empleado = {
      idempleado: null,
      nombres: '',
      ap_paterno: '',
      ap_materno: '',
      cargo_idcargo: null,
      complejo_idcomplejo: null,
      idcargo: null,
      cargo: ''
    };
  }

}
