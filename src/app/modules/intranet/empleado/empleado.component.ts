import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { Empleado } from '../../../shared/models/empleado.model';
import { EmpleadoService } from '../../../core/services/empleado.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.css']
})
export class EmpleadoComponent implements OnInit {

  empleado: Empleado[];
  constructor(
    private empleadoService: EmpleadoService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): any{
    this.listarEmpleados();
  }

  listarEmpleados(): void{
    this.empleadoService.listar().pipe(first()).subscribe(empleado => {
      if (empleado.success === true && empleado.data) {
        this.empleado = empleado.data;
      }
    }, error => {
      console.log(error);
    });
  }

  deleteEmpleado(id): void {
    this.empleadoService.eliminar(id)
      .subscribe(
        response => {
          this.listarEmpleados();
        },
        error => {
          console.log(error);
        });
  }

}
