import { Component, OnInit } from '@angular/core';
import {Equipamento} from '../../../shared/models/equipamento';
import {EquipamentoService} from '../../../core/services/equipamento.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-equipamento-details',
  templateUrl: './equipamento-details.component.html',
  styleUrls: ['./equipamento-details.component.css']
})
export class EquipamentoDetailsComponent implements OnInit {

  message = '';
  equipamento: Equipamento;
  submitted = false;

  constructor(
    private equipamentoService: EquipamentoService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): any {
    this.equipamento = {
      idequipamento: null,
      equipamento: ''
    };

    this.message = '';
    this.getEquipamento(this.route.snapshot.paramMap.get('id'));
  }

  getEquipamento(id): void {
    this.equipamentoService.equipamentoId(id)
      .subscribe(equipamento => {
        if (equipamento.success === true && equipamento.data) {
          this.equipamento = equipamento.data;
        }
      }, error => {
        console.log(error);
      });
  }

  updateEquipamento(): void {
    this.equipamentoService.actualizar(this.equipamento.idequipamento, this.equipamento)
      .subscribe(
        equipamento => {
          if (equipamento.success === true) {
            this.submitted = true;
            this.message = '¡Actualización con éxito!';
          }
        },
        error => {
          console.log(error);
        });
  }

}
