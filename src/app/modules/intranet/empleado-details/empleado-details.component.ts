import { Component, OnInit } from '@angular/core';
import { Empleado } from '../../../shared/models/empleado.model';
import { Cargo } from '../../../shared/models/cargo';
import { EmpleadoService } from '../../../core/services/empleado.service';
import { CargoService } from '../../../core/services/cargo.service';
import { first } from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-empleado-details',
  templateUrl: './empleado-details.component.html',
  styleUrls: ['./empleado-details.component.css']
})
export class EmpleadoDetailsComponent implements OnInit {

  message = '';
  empleado: Empleado;
  cargo: Cargo[];
  submitted = false;

  constructor(
    private empleadoService: EmpleadoService,
    private route: ActivatedRoute,
    private router: Router,
    private cargoService: CargoService) { }

  ngOnInit(): any {
    this.empleado = {
      idempleado: null,
      nombres: '',
      ap_paterno: '',
      ap_materno: '',
      cargo_idcargo: null,
      complejo_idcomplejo: null,
      idcargo: null,
      cargo: ''
    };

    this.cargoService.listar().pipe(first()).subscribe(cargo => {
      if (cargo.success === true && cargo.data) {
        this.cargo = cargo.data;
        console.log(this.cargo);
      }
    }, error => {
      console.log(error);
    });
    this.message = '';
    this.getEmpleado(this.route.snapshot.paramMap.get('id'));
  }

  getEmpleado(id): void {
    this.empleadoService.empleadoId(id)
      .subscribe(empleado => {
        if (empleado.success === true && empleado.data) {
          this.empleado = empleado.data;
        }
      }, error => {
        console.log(error);
      });
  }

  updateEmplado(): void {
    this.empleadoService.actualizar(this.empleado.idempleado, this.empleado)
      .subscribe(
        empleado => {
          if (empleado.success === true) {
            this.submitted = true;
            this.message = '¡Actualización con éxito!';
          }
        },
        error => {
          console.log(error);
        });
  }

}
