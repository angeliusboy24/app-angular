import { Component, OnInit } from '@angular/core';
import {Equipamento} from '../../../shared/models/equipamento';
import {EquipamentoService} from '../../../core/services/equipamento.service';
import {ActivatedRoute, Router} from '@angular/router';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-equipamento',
  templateUrl: './equipamento.component.html',
  styleUrls: ['./equipamento.component.css']
})
export class EquipamentoComponent implements OnInit {

  equipamento: Equipamento[];
  constructor(
    private equipamentoService: EquipamentoService) { }

  ngOnInit(): any{
    this.listarEquipamento();
  }

  listarEquipamento(): void{
    this.equipamentoService.listar().pipe(first()).subscribe(equipamento => {
      if (equipamento.success === true && equipamento.data) {
        this.equipamento = equipamento.data;
      }
    }, error => {
      console.log(error);
    });
  }

  deleteEquipamento(id): void {
    this.equipamentoService.eliminar(id)
      .subscribe(
        response => {
          this.listarEquipamento();
        },
        error => {
          console.log(error);
        });
  }

}
